import Joi = require('joi');
export const citySchemaValidator = Joi.object({
    city: Joi.string()
    //$ asserts position at the end of the string
        .pattern(new RegExp(/^[a-zA-Z]*$/))
        .required(),
    radius: Joi.number()
        .integer()
        .min(10)
        .max(5000)
        .optional()
})
export const businessesNumberValidator = Joi.object({
    businessesNumber: Joi.string()
        .pattern(new RegExp(/^[0-9]/))
        .required()
})









