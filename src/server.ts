//Server setup
import { Sequelize } from "sequelize";
import { Controller } from "./controller/controller";
let express = require('express');
export class Server {
    protected app;
    sequelize: any;
    constructor() {
        this.app = express();
        //check libraries of express
        this.app.use(express.json());
        let PORT = 3000;
        new Controller(this.app);
        this.app.listen(PORT, () => {
            console.log(`Server running on port ${PORT}`);
        });
    }
    //Connection to db one single time
    async dbConnection() {
        this.sequelize = new Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        })
    }
}