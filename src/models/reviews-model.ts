import { QueryTypes, Sequelize } from "sequelize";
import { businessesIds, Ids } from "../interfaces";
export class Reviews {
    sequelize: any;
    constructor() {
        this.sequelize = new Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        })
    }
    async selectRandomIdBusinesses(randomNumberOfBusinesses:number):Promise<businessesIds[]> {
        let idForBusiness: string = (`SELECT id, yelp_business_id  FROM businesses ORDER BY RAND() LIMIT ${randomNumberOfBusinesses}`);
        let saveOfIds:businessesIds[] = await this.sequelize.query(idForBusiness, { type: QueryTypes.SELECT });
        return saveOfIds;
    }
}