import { QueryTypes, Sequelize } from "sequelize";
import {  CityInterface } from "../interfaces";
export class CityModel {
    sequelize: any;
    constructor() {
        this.sequelize = new Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        })
    }
    async insert(cityName:string): Promise<number[]> {
        let queryInsert = (`INSERT INTO cities(city_name) values ('${cityName}')`)
        return await this.sequelize.query(queryInsert, { type: QueryTypes.INSERT });
    }
    async selectByCityName(cityName:string): Promise<CityInterface[] | []> {
            let city: string = (`SELECT * FROM cities WHERE city_name LIKE '${cityName}'`);
            let savedCityData = await this.sequelize.query(city, { type: QueryTypes.SELECT });
            return savedCityData;
    }
    async selectById(cityId:string): Promise<CityInterface[] | []> {
        let city: string = (`SELECT id FROM cities WHERE city_name LIKE '${cityId}'`);
        let savedCityId = await this.sequelize.query(city, { type: QueryTypes.SELECT });
        return savedCityId;
    }
    async select():Promise<CityInterface[] | []>{
        let city = `(SELECT * FROM cities)`;
        let savedCities = await this.sequelize.query(city, {type:QueryTypes.SELECT});
        return savedCities;
    }
}
