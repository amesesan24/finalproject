import { Sequelize } from "sequelize"
import { BusinesId, BusinessesId, Ids, Keywords, KeywordsAndBusinessId, WordsAndBusinessId } from "../interfaces";
export class BusinessesKeywordsModel {
    sequelize: any;
    constructor() {
        this.sequelize = new Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        })
    }
    async insertBusinesKeywords(objectOfIds:Ids): Promise<void> {
        let insertToBusinesKeywords = `INSERT INTO businesses_keywords(businesses_id, keywords_id ) VALUES (${objectOfIds.id_business}, ${objectOfIds.word_id})`
        await this.sequelize.query(insertToBusinesKeywords);
    }
    async select(){
        let select = `(SELECT * FROM businesses_keywords)`
       let businessKeywordData =  await this.sequelize.query(select);
       return businessKeywordData;
    }
    async selectByWordIdAndBusinessID(objectOfIds:Ids):Promise<Ids[] | []>{
        let select = `(SELECT keywords_id, businesses_id FROM businesses_keywords WHERE keywords_id=${objectOfIds.word_id} AND businesses_id=${objectOfIds.id_business})`
        let wordAndBusinessId = await this.sequelize.query(select)
        return wordAndBusinessId[0];
    }
    async selectCount():Promise<Keywords[] | []>{
        let response =  `(SELECT keywords_id, COUNT(businesses_id) AS numberOfBusinesses FROM businesses_keywords GROUP BY keywords_id);`;
        let wordAndBusinessId = await this.sequelize.query(response)
        return wordAndBusinessId[0];
    }
    async selectByKeywordId(keyword_id:Keywords):Promise<[] | WordsAndBusinessId[][]>{
        let response = `(SELECT * FROM businesses_keywords INNER JOIN keywords ON businesses_keywords.keywords_id=keywords.id WHERE businesses_keywords.keywords_id=${keyword_id.keywords_id})`
        let word = await this.sequelize.query(response)
        return word[0][0]
    }
    async selectBusinessId(keyword_id:Keywords):Promise<BusinessesId[] | []>{
        let response = `(SELECT businesses_id FROM businesses_keywords WHERE keywords_id=${keyword_id.keywords_id})`
        let businessId = await this.sequelize.query(response)
        return businessId[0];
    }
    async selectByBusinessId(businessId:BusinesId):Promise<KeywordsAndBusinessId[] | []>{
        let response = `(SELECT keywords_id,businesses_id FROM businesses_keywords WHERE businesses_id=${businessId.id})`
        let keywords = await this.sequelize.query(response);
        if(keywords.length < 1){
            throw new Error(`For this business Id you don't data avaible. Please update your database for this businessId:${businessId.id}`)
        }
        return keywords[0];
    }

}

