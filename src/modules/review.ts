import { Sequelize } from "sequelize";
import { businessesIds, Ids, WordsAndBusinesId, YelpDataReview } from "../interfaces";
import { Keywords } from "../models/keywords-model";
export class Review {
    sequelize: any;
    constructor() {
        this.sequelize = new Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        })
    }
    //-------------------------------------// words filtration
    async filterKeywords(reviews:YelpDataReview[]): Promise<string[] | []> {
        try {
            let words: string[] = await this.extractKeywordsFromReview(reviews);
            if (!words) {
                return [];
            }
            let savedWords: string[] = [];
            for (let validWords of words) {
                if (validWords.length > 5 && validWords.length < 8) {
                    savedWords.push(validWords);
                }
            }
            return savedWords;
        } catch (error) {
            throw new Error(error.message);
        }
    }
    // -----------------------------------// words extraction
    async extractKeywordsFromReview(reviews:YelpDataReview[]): Promise<string[] | []> {
        try {
            let finalDataAfterFiltration: string[] = [];
            for (let reviewsObject of reviews) {
                let textFromReview = reviewsObject.text;
                const regexMatch = /[a-zA-Z'ÁÉÍÓÚáéíóúâêîôûàèìòùÇç]+/gm;
                let resultFromRegex = textFromReview.match(regexMatch)
                if (resultFromRegex == null) {
                    continue;
                }
                if (resultFromRegex.length >= 1) {
                    for (let words of resultFromRegex) {
                        let processedDataReviews = `${words.toLowerCase()}`;
                        finalDataAfterFiltration.push(processedDataReviews);
                    }
                }
            }
            return finalDataAfterFiltration;
        } catch (error) {
            throw new Error(error.message);
        }
    }
    async keywordsModul(words:WordsAndBusinesId[]): Promise<Ids[]> {
        try {
            let arrayOfIds: Ids[] = []
            let saveIds: any
            if (words) {
                for (let objectOfWords of words) {
                    for (let word of objectOfWords["words"]) {
                        let selectIdKeyword = new Keywords()
                        let idOfWord = await selectIdKeyword.select(word)
                        if (idOfWord.length < 1) {
                            let finalWord = `("${word}")`
                            let insertedWord = await selectIdKeyword.insert(finalWord)
                            saveIds = { "word_id": insertedWord[0], "id_business": objectOfWords["id_busines"]}
                        } else {
                            saveIds = { "word_id": idOfWord[0]["id"], "id_business": objectOfWords["id_busines"]};
                        }
                        arrayOfIds.push(saveIds);
                    }
                }
            }
            return arrayOfIds;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}