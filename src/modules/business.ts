import { Sequelize } from "sequelize";
import { CityRadius, YelpBusinessData } from "../interfaces";
import { BusinessModel } from "../models/businesses-model";
import { CityModel } from "../models/cities-model";
import { Yelp } from "../yelp-api/yelp";
export class Business {
    city: string;
    radius: number;
    sequelize: any;
    constructor(city: string, radius: number) {
        this.city = city;
        this.radius = radius;
        this.sequelize = new Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        })
    }
    async insertBusinessExternal(cityObject:CityRadius):Promise<YelpBusinessData[] | []>{
        try {
            let cityModel = new CityModel()
            let cityId = await cityModel.selectByCityName(cityObject.city);
            if(cityId.length < 1){
                throw new Error("This city don't exist in tabel of Cities. Make sure to insert this city in the database first time to take to information about the businnesses.")
            }
            let yelp = new Yelp();
            let businesses = await yelp.businessSearch(cityObject);
            if (businesses == null) {
                throw new Error("Data from YELP it's not avaible.");
            }
            let businessesData = []
            let businessModel = new BusinessModel();
                 if(businesses.length > 0){
                for (let business of businesses) {
                    if (business.rating >= 3 && business.rating <= 5 && business.review_count >= 10) {
                        let businessData =  await businessModel.selectByYelpBusinessId(business.id);
                        if(businessData.length == 0){
                        let processedBusinessData = `(${cityId[0]["id"]},"${business.id}","${business.alias}","${business.name}",${business.review_count},${business.rating},"${business.location.address1}", ${business.location.zip_code}, "${business.location.country}","${business.location.display_address}")`;
                         businessesData.push({
                            cities_id: cityId[0]["id"],
                            yelp_business_id:business.id,
                            alias:business.alias,
                            name:business.name,
                            review_count:business.review_count,
                            rating:business.rating,
                            addres:business.location.address1,
                            zip_code:business.location.zip_code,
                            country:business.location.country,
                            display_addres:business.location.display_address,
                        });
                         await businessModel.insert(processedBusinessData)
                        }
                    }
                }
                return businessesData;
            }
        } catch (error) {
            throw new Error(error.message);
        }
    }
}



