import { Sequelize } from "sequelize";
import { CityModel } from "../models/cities-model";
export class City {
    city: string;
    sequelize: any;
    constructor(city: string) {
        this.city = city;
        this.sequelize = new Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        })
    }
    async insertCity(city:string): Promise<boolean> {
        try {
            let cityModel = new CityModel();
            let cityData = await cityModel.selectByCityName(city);
            if (cityData.length < 1) {
                await cityModel.insert(city);
                return true;
            }
            return false;
        } catch (error) {
            throw new Error(error.message);
        }
    }
}
