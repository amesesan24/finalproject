import axios from "axios";
import { businessesIds, CityRadius, YelpBusinessData, YelpDataReview } from "../interfaces";
export class Yelp {
    config = {
        headers: {
            Authorization: 'Bearer BAqXq0kGgwU11qcdUdMn8DaUysDlViqHCkSnXd1alWaURGlRHfSI74XaJM1HxuAm5z91Ld9ODN55ouJu-_yI2Y2OwET94eYaSKikmMYNiHC14EYraPuZsZ0svDDHYnYx'
        }
    };
    constructor() {}
    async businessSearch(cityAndRadius:CityRadius):Promise<YelpBusinessData[] | []>  {
        let url = `https://api.yelp.com/v3/businesses/search?location=${cityAndRadius.city}&radius=${cityAndRadius.radius}`
        let response = await axios.get(url, this.config);
        return response.data.businesses
    }
    async reviews(business:businessesIds): Promise<YelpDataReview[]> {
           let url = `https://api.yelp.com/v3/businesses/${business["yelp_business_id"]}/reviews`
           let response = await axios.get(url, this.config);
           return response.data.reviews
        }
}
