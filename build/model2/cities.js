"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cities = void 0;
const sequelize_1 = require("sequelize");
class Cities {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async insertCity(cityObject) {
        let queryInsert = (`INSERT INTO cities(city_name) values ('${cityObject.city}')`);
        await this.sequelize.query(queryInsert, { type: sequelize_1.QueryTypes.INSERT });
    }
    async selectCityId(cityObject) {
        try {
            let cityId = (`SELECT id FROM cities WHERE city_name LIKE '${cityObject.city}'`);
            let savedCityId = await this.sequelize.query(cityId, { type: sequelize_1.QueryTypes.SELECT });
            if (savedCityId.length == 1) {
                return savedCityId[0]["id"];
            }
            else if (savedCityId.length < 1) {
                return false;
            }
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
}
exports.Cities = Cities;
//# sourceMappingURL=cities.js.map