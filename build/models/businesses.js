"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BusinessModel = void 0;
const sequelize_1 = require("sequelize");
class BusinessModel {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async insert(businessesInformation) {
        let insertToBusinessTable = `INSERT INTO businesses(cities_id, id_business,alias, name, review_count, rating, address, zip_code, country, display_address) VALUES ${businessesInformation}`;
        await this.sequelize.query(insertToBusinessTable);
    }
    async selectByCityId(cityIdObject) {
        let data = `SELECT * FROM businesses WHERE cities_id='${cityIdObject[0]["id"]}'`;
        let dataFromBusinesses = await this.sequelize.query(data, { type: sequelize_1.QueryTypes.SELECT });
        return dataFromBusinesses;
    }
}
exports.BusinessModel = BusinessModel;
//# sourceMappingURL=businesses.js.map