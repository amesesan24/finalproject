"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BusinessModel = void 0;
const sequelize_1 = require("sequelize");
class BusinessModel {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async insert(businessesInformation) {
        let insertToBusinessTable = `INSERT INTO businesses(cities_id, yelp_business_id,alias, name, review_count, rating, address, zip_code, country, display_address) VALUES ${businessesInformation}`;
        await this.sequelize.query(insertToBusinessTable);
    }
    async selectByCityId(city) {
        let data = `SELECT id FROM businesses WHERE cities_id='${city.id}'`;
        let businessId = await this.sequelize.query(data, { type: sequelize_1.QueryTypes.SELECT });
        if (businessId.length < 1) {
            throw new Error(`You don't have any businesses inserted for this city:${city.city_name}.Please update you're businesses table.`);
        }
        return businessId;
    }
    async selectByYelpBusinessId(yelpBusinessId) {
        let data = `SELECT * FROM businesses WHERE yelp_business_id='${yelpBusinessId}'`;
        let dataFromBusinesses = await this.sequelize.query(data, { type: sequelize_1.QueryTypes.SELECT });
        return dataFromBusinesses;
    }
}
exports.BusinessModel = BusinessModel;
//# sourceMappingURL=businesses-model.js.map