"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CityModel = void 0;
const sequelize_1 = require("sequelize");
class CityModel {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async insert(cityName) {
        let queryInsert = (`INSERT INTO cities(city_name) values ('${cityName}')`);
        return await this.sequelize.query(queryInsert, { type: sequelize_1.QueryTypes.INSERT });
    }
    async selectByCityName(cityName) {
        let city = (`SELECT * FROM cities WHERE city_name LIKE '${cityName}'`);
        let savedCityData = await this.sequelize.query(city, { type: sequelize_1.QueryTypes.SELECT });
        return savedCityData;
    }
    async selectById(cityId) {
        let city = (`SELECT id FROM cities WHERE city_name LIKE '${cityId}'`);
        let savedCityId = await this.sequelize.query(city, { type: sequelize_1.QueryTypes.SELECT });
        return savedCityId;
    }
    async select() {
        let city = `(SELECT * FROM cities)`;
        let savedCities = await this.sequelize.query(city, { type: sequelize_1.QueryTypes.SELECT });
        return savedCities;
    }
}
exports.CityModel = CityModel;
//# sourceMappingURL=cities-model.js.map