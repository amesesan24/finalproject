"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiCall = void 0;
let axios = require('axios');
class ApiCall {
    constructor() {
        this.apiKey = "BAqXq0kGgwU11qcdUdMn8DaUysDlViqHCkSnXd1alWaURGlRHfSI74XaJM1HxuAm5z91Ld9ODN55ouJu-_yI2Y2OwET94eYaSKikmMYNiHC14EYraPuZsZ0svDDHYnYx";
        this.url = "https://api.yelp.com/v3/businesses/search?";
    }
    ;
    async responseFromApi() {
        let finalUrl = `${this.url}${this.type}&key=${this.apiKey}`;
        return await axios.get(finalUrl);
    }
}
exports.ApiCall = ApiCall;
//# sourceMappingURL=api-call.js.map