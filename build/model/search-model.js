"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SearchModel = void 0;
const sequelize_1 = require("sequelize");
const api_call_search_1 = require("../api-call/api-call-search");
const insert_businesses_1 = require("./businesses/insert-businesses");
const select_businesses_1 = require("./businesses/select-businesses");
class SearchModel {
    constructor(city, radius) {
        this.city = city;
        this.radius = radius;
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async businessesModel(cityAndRadius) {
        try {
            let ApiCall = new api_call_search_1.ApiCallForSearch(this.city, this.radius);
            let responseOfApiCall = await ApiCall.responseFromApi();
            if (!responseOfApiCall) {
                throw new Error("Check you API request");
            }
            let ifExistAlready = new select_businesses_1.SelectBusinesses();
            let cityId = await ifExistAlready.selectBusinesses(cityAndRadius);
            let bulkedData = '';
            for (let business of responseOfApiCall) {
                let processedBusinessData = `(${cityId},"${business.id}","${business.alias}","${business.name}",${business.review_count},${business.rating},"${business.location.address1}", ${business.location.zip_code}, "${business.location.country}","${business.location.display_address}")`;
                if (business.rating >= 3 && business.rating <= 5 && business.review_count >= 10) {
                    bulkedData = bulkedData.concat(processedBusinessData, ',');
                }
            }
            bulkedData = bulkedData.slice(0, -1);
            let insert = new insert_businesses_1.InsertBusinesses();
            await insert.insertBusinesses(bulkedData);
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
}
exports.SearchModel = SearchModel;
//# sourceMappingURL=search-model.js.map