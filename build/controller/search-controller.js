"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Controllers = void 0;
const insert_city_db_1 = require("../model/insert-city-db");
const search_model_1 = require("../model/search-model");
class Controllers {
    constructor(router) {
        this.router = router;
        this.router.get('/get', async (req, res) => {
            let queryParams = new insert_city_db_1.InsertCityInDB(req.query.city);
            await queryParams.verifyCityFromDB();
            res.send(200, {
                message: "Database upload successfuly"
            });
            res.send(200, {
                message: "Duplicate entry for this city"
            });
        });
        this.router.post('/search', async (req, res) => {
            try {
                //De modificat codul pt tabela de Cities si pus ambele controlere intr o singura parte. 
                let modelOfSearch = new search_model_1.SearchModel(req.body.city, req.body.radius);
                let verify = await modelOfSearch.databaseForSearch();
                if (verify == true) {
                    res.send(200, {
                        message: "Data already exists in database"
                    });
                }
                else {
                    res.send(200, {
                        message: "Data successfully updated in database"
                    });
                }
            }
            catch (err) {
                console.log(err);
            }
        });
    }
}
exports.Controllers = Controllers;
//# sourceMappingURL=search-controller.js.map