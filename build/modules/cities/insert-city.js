"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.InsertCity = void 0;
const sequelize_1 = require("sequelize");
class InsertCity {
    constructor() {
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async insertCity(city) {
        let queryInsert = (`INSERT INTO cities(city_name) values ('${city.city}')`);
        await this.sequelize.query(queryInsert, { type: sequelize_1.QueryTypes.INSERT });
    }
}
exports.InsertCity = InsertCity;
//# sourceMappingURL=insert-city.js.map