"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.City = void 0;
const sequelize_1 = require("sequelize");
const cities_model_1 = require("../models/cities-model");
class City {
    constructor(city) {
        this.city = city;
        this.sequelize = new sequelize_1.Sequelize('mentorship', 'adrian', 'mentorat', {
            host: 'localhost',
            dialect: 'mysql',
            timezone: '+03:00'
        });
    }
    async insertCity(city) {
        try {
            let cityModel = new cities_model_1.CityModel();
            let cityData = await cityModel.selectByCityName(city);
            if (cityData.length < 1) {
                await cityModel.insert(city);
                return true;
            }
            return false;
        }
        catch (error) {
            throw new Error(error.message);
        }
    }
}
exports.City = City;
//# sourceMappingURL=city.js.map